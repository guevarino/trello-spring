package it.fides.trello.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fides.trello.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
