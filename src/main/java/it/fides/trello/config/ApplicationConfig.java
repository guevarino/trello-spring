package it.fides.trello.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
public class ApplicationConfig {
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
}
