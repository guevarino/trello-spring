package it.fides.trello.dto;

import java.time.LocalDate;

public class UserDTO {
	
	int idUser;
    int idRole;
    String username;
    LocalDate birthday;
    String mail;
    int phoneNumber;
    LocalDate dateRegistration;
    LocalDate dateLastAccess;
    int flagDelete;
    
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdRole() {
		return idRole;
	}
	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public LocalDate getDateRegistration() {
		return dateRegistration;
	}
	public void setDateRegistration(LocalDate dateRegistration) {
		this.dateRegistration = dateRegistration;
	}
	public LocalDate getDateLastAccess() {
		return dateLastAccess;
	}
	public void setDateLastAccess(LocalDate dateLastAccess) {
		this.dateLastAccess = dateLastAccess;
	}
	public int getFlagDelete() {
		return flagDelete;
	}
	public void setFlagDelete(int flagDelete) {
		this.flagDelete = flagDelete;
	}
	
	
}
