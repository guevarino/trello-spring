package it.fides.trello.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDate;

@Entity
@Table(name = "users")
public class User {
	
	@Id
    @GeneratedValue (strategy = GenerationType.AUTO	)
	int idUser;
	
	@Column (name = "id_role", nullable = false)
    int idRole;
    
	@Column (name = "username", nullable = false)
    String username;
	
	@Column (name = "birthday", nullable = false)
    LocalDate birthday;
	
	@Column (name = "mail", nullable = false)
    String mail;
	
	@Column (name = "phone_number", nullable = false)
    int phoneNumber;
	
	@Column (name = "date_registration", nullable = false)
    LocalDate dateRegistration;
	
	@Column (name = "last_access", nullable = false)
    LocalDate dateLastAccess;
	
	@Column (name = "flag_delete ", nullable = false)
    int flagDelete;    
    
    public User() {}
    
    public User(int idUser, int idRole, String username, LocalDate birthday, String mail, int phoneNumber,
    		     LocalDate dateRegistration, LocalDate dateLastAccess, int flagDelete) {
	this.idUser = idUser;
	this.idRole = idRole;
	this.username = username;
	this.birthday = birthday;
	this.mail = mail;
	this.phoneNumber = phoneNumber;
	this.dateRegistration = dateRegistration;
	this.dateLastAccess = dateLastAccess;
	this.flagDelete = flagDelete;
}

    public int getIdUser() {
		return idUser;
	}
    
    

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
	public LocalDate getDateRegistration() {
		return dateRegistration;
	}

	public void setDateRegistration(LocalDate dateRegistration) {
		this.dateRegistration = dateRegistration;
	}
	
	
	
	public LocalDate getDateLastAccess() {
		return dateLastAccess;
	}

	public void setDateLastAccess(LocalDate dateLastAccess) {
		this.dateLastAccess = dateLastAccess;
	}
	
	
	public int getFlagDelete() {
		return flagDelete;
	}

	public void setFlagDelete(int flagDelete) {
		this.flagDelete = flagDelete;
	}
    
	
}