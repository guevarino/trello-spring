package it.fides.trello.converter;

import org.modelmapper.ModelMapper;

import it.fides.trello.dto.UserDTO;
import it.fides.trello.model.User;



public class Converter {
	
	ModelMapper modelMapper;
	
	public User convertToUserEntity(UserDTO userDto) {
	    User user = modelMapper.map(userDto, User.class);
	    return user;
	}
	
	public UserDTO convertToUserDto(User user) {
	    UserDTO userDto = modelMapper.map(user, UserDTO.class);
	    return userDto;
	}
}
